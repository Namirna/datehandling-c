﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moosetrail.DateHandling.DateTimeExtensions;

namespace Moosetrail.DateHandling.UnitTests.DateTimeExtensions
{
    [TestClass]
    public class ConvertDate_Specs
    {
        #region AsDate

        [TestMethod]
        public void asDate_should_transfer_year()
        {
            // Given 
            var date = new DateTime(2000, 10, 15);

            // When 
            var result = date.AsDate();

            // Then
            Assert.AreEqual(2000, result.Year);
        }
        [TestMethod]
        public void asDate_should_transfer_month()
        {
            // Given 
            var date = new DateTime(2000, 10, 15);

            // When 
            var result = date.AsDate();

            // Then
            Assert.AreEqual(10, result.Month);
        }
        [TestMethod]
        public void asDate_should_transfer_day()
        {
            // Given 
            var date = new DateTime(2000, 10, 15);

            // When 
            var result = date.AsDate();

            // Then
            Assert.AreEqual(15, result.Day);
        }



        #endregion AsDate
    }
}
