﻿using System;
using System.Globalization;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Moosetrail.DateHandling.UnitTests
{
    [TestClass]
    public class Date_Specs
    {
        private Date SUT;

        [TestInitialize]
        public void Setup()
        {
            SUT = new Date();
        }

        [TestCleanup]
        public void Teardown()
        {
            SUT = null;
        }

        private void setEnglishUI()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
        }

        #region Construct year, month, day

        [TestMethod]
        public void constructorYMD_should_set_year()
        {
            // When 
            SUT = new Date(2000, 10, 20);

            // Then
            Assert.AreEqual(2000, SUT.Year);
        }

        [TestMethod]
        public void constructorYMD_should_set_month()
        {
            // When 
            SUT = new Date(2000, 10, 20);

            // Then
            Assert.AreEqual(10, SUT.Month);
        }

        [TestMethod]
        public void constructorYMD_should_set_day()
        {
            // When 
            SUT = new Date(2000, 10, 20);

            // Then
            Assert.AreEqual(20, SUT.Day);
        }

        [TestMethod]
        public void constructorYMD_should_throw_if_year_is_negative()
        {
            // Given
            setEnglishUI();

            try
            {
                //When
                SUT = new Date(-1, 10, 20);
                Assert.Fail("Exception expected, none thrown");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Then
                Assert.AreEqual("The year must be in the interval 1-9999, 0 represents empty\r\nParameter name: year\r\nActual value was -1.", ex.Message);
            }
        }
        [TestMethod]
        public void constructorYMD_should_throw_if_year_is_to_big()
        {
            // Given
            setEnglishUI();

            try
            {
                //When
                SUT = new Date(10000, 10, 20);
                Assert.Fail("Exception expected, none thrown");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Then
                Assert.AreEqual("The year must be in the interval 1-9999, 0 represents empty\r\nParameter name: year\r\nActual value was 10000.", ex.Message);
            }
        }

        [TestMethod]
        public void constructorYMD_should_throw_if_month_is_negative()
        {
            setEnglishUI();

            try
            {
                //When
                SUT = new Date(2000, -1, 20);
                Assert.Fail("Exception expected, none thrown");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Then
                Assert.AreEqual("The month must be in the interval 1-12, 0 represents empty\r\nParameter name: month\r\nActual value was -1.", ex.Message);
            }
        }
        [TestMethod]
        public void constructorYMD_should_throw_if_month_is_to_big()
        {
            setEnglishUI();

            try
            {
                //When
                SUT = new Date(2000, 13, 20);
                Assert.Fail("Exception expected, none thrown");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Then
                Assert.AreEqual("The month must be in the interval 1-12, 0 represents empty\r\nParameter name: month\r\nActual value was 13.", ex.Message);
            }
        }
        [TestMethod]
        public void constructorYMD_should_throw_if_day_is_negative()
        {
            setEnglishUI();

            try
            {
                //When
                SUT = new Date(2000, 1, -1);
                Assert.Fail("Exception expected, none thrown");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Then
                Assert.AreEqual("The day must be in the interval 1 to the number of days in the month, 0 represents empty\r\nParameter name: day\r\nActual value was -1.", ex.Message);
            }
        }
        [TestMethod]
        public void constructorYMD_should_throw_if_day_is_to_big()
        {
            setEnglishUI();

            try
            {
                //When
                SUT = new Date(2000, 1, 32);
                Assert.Fail("Exception expected, none thrown");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Then
                Assert.AreEqual("The day must be in the interval 1 to the number of days in the month, 0 represents empty\r\nParameter name: day\r\nActual value was 32.", ex.Message);
            }
        }

        #endregion Construct year, month, day

        #region Constructor string

        [TestMethod]
        public void constructorString_should_set_year()
        {
            // When 
            SUT = new Date("2000-10-20");

            // Then
            Assert.AreEqual(2000, SUT.Year);
        }
        [TestMethod]
        public void constructorString_should_set_month()
        {
            // When 
            SUT = new Date("2000-10-20");

            // Then
            Assert.AreEqual(10, SUT.Month);
        }
        [TestMethod]
        public void constructorString_should_set_day()
        {
            // When 
            SUT = new Date("2000-10-20");

            // Then
            Assert.AreEqual(20, SUT.Day);
        }
        [TestMethod]
        public void constructorString_should_throw_if_year_is_negative()
        {
            // Given
            setEnglishUI();

            try
            {
                //When
                SUT = new Date("-1-10-20");
                Assert.Fail("Exception expected, none thrown");
            }
            catch (FormatException ex)
            {
                // Then
                Assert.AreEqual("The string wasn't formated as a date that this class can handle", ex.Message);
            }
        }
        [TestMethod]
        public void constructorString_should_throw_if_year_is_to_big()
        {
            // Given
            setEnglishUI();

            try
            {
                //When
                SUT = new Date("10000-10-20");
                Assert.Fail("Exception expected, none thrown");
            }
            catch (FormatException ex)
            {
                // Then
                Assert.AreEqual("The string wasn't formated as a date that this class can handle", ex.Message);
            }
        }

        [TestMethod]
        public void constructorString_should_throw_if_month_is_negative()
        {
            setEnglishUI();

            try
            {
                //When
                SUT = new Date("2001--1-20");
                Assert.Fail("Exception expected, none thrown");
            }
            catch (FormatException ex)
            {
                // Then
                Assert.AreEqual("The string wasn't formated as a date that this class can handle", ex.Message);
            }
        }
        [TestMethod]
        public void constructorString_should_throw_if_month_is_to_big()
        {
            setEnglishUI();

            try
            {
                //When
                SUT = new Date("2000-13-20");
                Assert.Fail("Exception expected, none thrown");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Then
                Assert.AreEqual("The month must be in the interval 1-12, 0 represents empty\r\nParameter name: month\r\nActual value was 13.", ex.Message);
            }
        }
        [TestMethod]
        public void constructorString_should_throw_if_day_is_negative()
        {
            setEnglishUI();

            try
            {
                //When
                SUT = new Date("2000-10--1");
                Assert.Fail("Exception expected, none thrown");
            }
            catch (FormatException ex)
            {
                // Then
                Assert.AreEqual("The string wasn't formated as a date that this class can handle", ex.Message);
            }
        }
        [TestMethod]
        public void constructorString_should_throw_if_day_is_to_big()
        {
            setEnglishUI();

            try
            {
                //When
                SUT = new Date("2000-10-32");
                Assert.Fail("Exception expected, none thrown");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                // Then
                Assert.AreEqual("The day must be in the interval 1 to the number of days in the month, 0 represents empty\r\nParameter name: day\r\nActual value was 32.", ex.Message);
            }
        }

        [TestMethod]
        public void constructorString_should_throw_if_string_isnt_date_formated()
        {
            try
            {
                //When
                SUT = new Date("my date");
                Assert.Fail("Exception expected, none thrown");
            }
            catch (FormatException ex)
            {
                // Then
                Assert.AreEqual("The string wasn't formated as a date that this class can handle", ex.Message);
            }
        }

        [TestMethod]
        public void constructorString_should_handle_yymmdd_to_set_year()
        {
            // When 
            SUT = new Date("10-11-12");

            // Then
            Assert.AreEqual(2010, SUT.Year);
        }
        [TestMethod]
        public void constructorString_should_handle_yymmdd_to_set_year_to_correct_centry()
        {
            // When 
            SUT = new Date("70-11-12");

            // Then
            Assert.AreEqual(1970, SUT.Year);
        }
        [TestMethod]
        public void constructorString_should_handle_yymdd_to_set_month()
        {
            // When 
            SUT = new Date("2010-6-12");

            // Then
            Assert.AreEqual(6, SUT.Month);
        }
        [TestMethod]
        public void constructorString_should_handle_yymd_to_set_month()
        {
            // When 
            SUT = new Date("10-6-3");

            // Then
            Assert.AreEqual(6, SUT.Month);
            Assert.AreEqual(2010, SUT.Year);
            Assert.AreEqual(3, SUT.Day);
        }

        #endregion Constructor string

        #region Today

        [TestMethod]
        public void today_should_set_year_to_today()
        {
            // When 
            SUT = Date.Today;

            // Then
            Assert.AreEqual(DateTime.Today.Year, SUT.Year);
        }
        [TestMethod]
        public void today_should_set_month_to_today()
        {
            // When 
            SUT = Date.Today;

            // Then
            Assert.AreEqual(DateTime.Today.Month, SUT.Month);
        }
        [TestMethod]
        public void today_should_set_day_to_today()
        {
            // When 
            SUT = Date.Today;

            // Then
            Assert.AreEqual(DateTime.Today.Day, SUT.Day);
        }

        #endregion Today

        #region AsDateTime

        [TestMethod]
        public void asDateTime_should_set_year()
        {
            // Given 
            SUT = new Date(2000, 11, 15);

            // When 
            var result = SUT.AsDateTime();

            // Then
            Assert.AreEqual(2000, result.Year);
        }
        [TestMethod]
        public void asDateTime_should_set_month()
        {
            // Given 
            SUT = new Date(2000, 11, 15);

            // When 
            var result = SUT.AsDateTime();

            // Then
            Assert.AreEqual(11, result.Month);
        }
        [TestMethod]
        public void asDateTime_should_set_day()
        {
            // Given 
            SUT = new Date(2000, 11, 15);

            // When 
            var result = SUT.AsDateTime();

            // Then
            Assert.AreEqual(15, result.Day);
        }

        #endregion AsDateTime

        #region Equals Date

        [TestMethod]
        public void equals_should_return_true_if_pointing_to_same_date()
        {
            // Given 
            var other = setSutAndOtherTo();

            // Then
            Assert.IsTrue(SUT.Equals(other));
        }

        private Date setSutAndOtherTo(int year = 2000, int month = 10, int day = 15)
        {
            SUT = new Date(2000, 10, 15);
            var other = new Date(year, month, day);
            return other;
        }

        [TestMethod]
        public void equals_should_return_false_if_years_arent_same()
        {
            // Given 
            var other = setSutAndOtherTo(2014);

            // Then
            Assert.IsFalse(SUT.Equals(other));
        }

        [TestMethod]
        public void equals_should_return_false_if_months_arent_same()
        {
            // Given 
            var other = setSutAndOtherTo(2000, 1);

            // Then
            Assert.IsFalse(SUT.Equals(other));
        }
        [TestMethod]
        public void equals_should_return_false_if_days_arent_same()
        {
            // Given 
            var other = setSutAndOtherTo(2000, 10, 10);

            // Then
            Assert.IsFalse(SUT.Equals(other));
        }

        [TestMethod]
        public void equals_should_return_false_if_other_is_null()
        {
            // Then
            Assert.IsFalse(SUT.Equals(null));
        }

        #endregion Equals Date

        #region Equals DateTime

        [TestMethod]
        public void equals_dateTime_should_return_true_if_dates_are_same()
        {
            // Given 
            var other = setSutAndDateTime();

            // Then
            Assert.IsTrue(SUT.Equals(other));
        }

        private DateTime setSutAndDateTime(int year = 2000, int month = 10, int day = 15)
        {
            SUT = new Date(2000, 10, 15);
            var other = new DateTime(year, month, day);
            return other;
        }

        [TestMethod]
        public void equals_dateTime_should_return_false_if_years_arent_same()
        {
            // Given 
            var other = setSutAndDateTime(2014);

            // Then
            Assert.IsFalse(SUT.Equals(other));
        }

        [TestMethod]
        public void equals_dateTime_should_return_false_if_months_arent_same()
        {
            // Given 
            var other = setSutAndDateTime(2000, 1);

            // Then
            Assert.IsFalse(SUT.Equals(other));
        }
        [TestMethod]
        public void equals_dateTime_should_return_false_if_days_arent_same()
        {
            // Given 
            var other = setSutAndDateTime(2000, 10, 10);

            // Then
            Assert.IsFalse(SUT.Equals(other));
        }

        [TestMethod]
        public void equals_dateTime_should_return_false_if_other_is_empty()
        {
            // Then
            Assert.IsFalse(SUT.Equals(new DateTime()));
        }

        #endregion Equals DateTime

        #region explicit DateTime

        [TestMethod]
        public void explicitDateTime_should_set_year()
        {
            // Given 
            SUT = new Date(2000, 11, 15);

            // When 
            var result = (DateTime)SUT;

            // Then
            Assert.AreEqual(2000, result.Year);
        }
        [TestMethod]
        public void explicitDateTime_should_set_month()
        {
            // Given 
            SUT = new Date(2000, 11, 15);

            // When 
            var result = (DateTime)SUT;

            // Then
            Assert.AreEqual(11, result.Month);
        }
        [TestMethod]
        public void explicitDateTime_should_set_day()
        {
            // Given 
            SUT = new Date(2000, 11, 15);

            // When 
            var result = (DateTime)SUT;

            // Then
            Assert.AreEqual(15, result.Day);
        }

        #endregion explicit

        #region >= Datetime, Date

        [TestMethod]
        public void eqOrGreater_should_return_true_if_the_dates_are_same()
        {
            // Given 
            var d1 = new DateTime(2000, 1, 1, 10, 5, 5);
            var d2 = new Date(2000, 1, 1);

            // Then
            Assert.IsTrue(d1 >= d2);
        }

        [TestMethod]
        public void eqOrGreater_should_return_true_if_datetime_is_greater()
        {
            // Given 
            var d1 = new DateTime(2000, 2, 5, 10, 5, 5);
            var d2 = new Date(2000, 1, 15);

            // Then
            Assert.IsTrue(d1 >= d2);
        }

        [TestMethod]
        public void eqOrGreater_should_return_false_if_datetime_is_less()
        {
            // Given 
            var d1 = new DateTime(1999, 1, 5, 10, 5, 5);
            var d2 = new Date(2000, 1, 1);

            // Then
            Assert.IsFalse(d1 >= d2);
        }

        #endregion >= Datetime, Date

        #region <= DateTime Date

        [TestMethod]
        public void eqOrLess_should_return_true_if_the_dates_are_same()
        {
            // Given 
            var d1 = new DateTime(2000, 1, 1, 10, 5, 5);
            var d2 = new Date(2000, 1, 1);

            // Then
            Assert.IsTrue(d1 <= d2);
        }

        [TestMethod]
        public void eqOrLess_should_return_false_if_datetime_is_greater()
        {
            // Given 
            var d1 = new DateTime(2000, 1, 5, 10, 5, 5);
            var d2 = new Date(2000, 1, 1);

            // Then
            Assert.IsFalse(d1 <= d2);
        }

        [TestMethod]
        public void eqOrLess_should_return_true_if_datetime_is_less()
        {
            // Given 
            var d1 = new DateTime(1999, 1, 5, 10, 5, 5);
            var d2 = new Date(2000, 1, 1);

            // Then
            Assert.IsTrue(d1 <= d2);
        }

        #endregion <= DateTime Date

    }
}
