﻿using System;

namespace Moosetrail.DateHandling.DateTimeExtensions
{
    /// <summary>
    /// Helper class that converts DateTime to Date
    /// </summary>
    public static class ConvertDate
    {
        /// <summary>
        /// Converts the value of the current DateTime object to the equivalent Date representation
        /// </summary>
        /// <param name="dt">The DateTime to convert</param>
        /// <returns>A Date representation of the value of the current DateTime object</returns>
        public static Date AsDate(this DateTime dt)
        {
            return new Date(dt.Year, dt.Month, dt.Day);
        }
    }
}
