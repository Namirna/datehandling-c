﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Moosetrail.DateHandling
{
    /// <summary>
    /// Represents a specific date 
    /// </summary>
    public class Date : IEquatable<Date>, IEquatable<DateTime>, IComparable<Date>, IComparable<DateTime>
    {
        /// <summary>
        /// Creates a new instance of the Date class to a specified year, month and day if any given
        /// </summary>
        /// <param name="year">The year of the date, 1 through 9999, default to empty</param>
        /// <param name="month">The month of the date, 1 through 12, defaut to empty</param>
        /// <param name="day">The day of the date, 1 through the number of days in month, default to empty</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if year, month or day is outside the allowed interval</exception>
        public Date(int year = 0, int month = 0, int day = 0)
        {
            createDate(year, month, day);
        }

        private void createDate(int year, int month, int day)
        {
            if (year < 0 || year > 9999)
                throw yearOutOfRangeException(year);

            if (month < 0 || month > 12)
                throw monthOutOfRangeException(month);

            if (day < 0 || dayIsOutsideMonth(day, month, year))
                throw dayOutOfRangeException(day);

            Year = year;
            Month = month;
            Day = day;
        }

        private static ArgumentOutOfRangeException yearOutOfRangeException(int year)
        {
            return new ArgumentOutOfRangeException("year", year,
                "The year must be in the interval 1-9999, 0 represents empty");
        }

        private static ArgumentOutOfRangeException monthOutOfRangeException(int month)
        {
            return new ArgumentOutOfRangeException("month", month,
                "The month must be in the interval 1-12, 0 represents empty");
        }
       
        private bool dayIsOutsideMonth(int day, int month, int year)
        {
            if (month == 0 && day == 0)
                return false;
            else switch (month)
            {
                case 9:
                case 6:
                case 4:
                case 11:
                    return day > 30;
                case 2:
                    return day > 28 || (DateTime.IsLeapYear(year) && day > 29);
                default:
                    return day > 31;
            }
        }

        private static ArgumentOutOfRangeException dayOutOfRangeException(int day)
        {
            return new ArgumentOutOfRangeException("day", day,
                "The day must be in the interval 1 to the number of days in the month, 0 represents empty");
        }


        /// <summary>
        /// Creates a new instance of the Date class to the specified date
        /// </summary>
        /// <param name="str">The string that represents a date to set to</param>
        /// <exception cref="ArgumentNullException">Thrown if the string is null, empty or whitespace</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the year, month or day given is outside the allowed interval</exception>
        public Date(string str)
        {
            if (!Regex.IsMatch(str, @"^\d{2,4}-(\d{1,2})-(\d{1,2})$"))
                throw new FormatException("The string wasn't formated as a date that this class can handle");

            var splitDate = str.Split(new[] {"-"}, StringSplitOptions.None);
            createDate(createYear(splitDate), Convert.ToInt32(splitDate[1]), Convert.ToInt32(splitDate[2]));
        }

        private static int createYear(string[] splitDate)
        {
            var year = Convert.ToInt32(splitDate[0]);

            if (splitDate[0].Length == 2)
            {
                var tempYear = year + 2000;
                if (tempYear > DateTime.Today.Year)
                    tempYear = year + 1900;
                year = tempYear;
            }

            return year;
        }

        /// <summary>
        /// Gets the year component of the date represented by this instance
        /// </summary>
        public int Year { get; private set; }

        /// <summary>
        /// Gets the month component of the date represented by this instance
        /// </summary>
        public int Month { get; private set; }

        /// <summary>
        /// Gets the day of the month represented by this instance
        /// </summary>
        public int Day { get; private set; }

        /// <summary>
        /// Gets the current date
        /// </summary>
        public static Date Today
        {
            get
            {
                var today = DateTime.Today;
                return new Date(today.Year, today.Month, today.Day);
            }
        }

        /// <summary>
        /// Converts the value of the current Date object to the equivalent DateTime representation 
        /// </summary>
        /// <returns>A DateTime representation of the value of the current Date object</returns>
        public DateTime AsDateTime()
        {
            return new DateTime(Year, Month, Day);
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified object  is equal to the current object; otherwise, false.
        /// </returns>
        /// <param name="obj">The object to compare with the current object. </param><filterpriority>2</filterpriority>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Date) obj);
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(Date other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Year == other.Year && Month == other.Month && Day == other.Day;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(DateTime other)
        {
            return Year == other.Year &&
                Month == other.Month &&
                Day == other.Day;
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other"/> parameter.Zero This object is equal to <paramref name="other"/>. Greater than zero This object is greater than <paramref name="other"/>. 
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public int CompareTo(Date other)
        {
            return CompareTo(other.AsDateTime());
        }

        /// <summary>
        /// Compares the current object with another object of the same type.
        /// </summary>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has the following meanings: Value Meaning Less than zero This object is less than the <paramref name="other"/> parameter.Zero This object is equal to <paramref name="other"/>. Greater than zero This object is greater than <paramref name="other"/>. 
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public int CompareTo(DateTime other)
        {
            var date = new DateTime(other.Year, other.Month, other.Day);
            return AsDateTime().CompareTo(date);
        }

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override int GetHashCode()
        {
            return AsDateTime().GetHashCode();
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public override string ToString()
        {
            return AsDateTime().ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the value of the current Date object to the equivalent DateTime representation 
        /// </summary>
        /// <param name="date">The Date to convert</param>
        /// <returns>A DateTime representation of the value of the current Date object</returns>
        public static explicit operator DateTime(Date date)
        {
            return date.AsDateTime();
        }

        /// <summary>
        /// Compares a DateTime to a Date and see if the DateTime is equal or greater than the Date
        /// </summary>
        /// <param name="date1">The date to compare</param>
        /// <param name="date2">The date to compare against</param>
        /// <returns>
        /// True if the DateTime is equal or greater than the Date, false otherwise
        /// </returns>
        public static bool operator >=(DateTime date1, Date date2)
        {
            if (date1.Year > date2.Year)
                return true;
            else if (date1.Year != date2.Year)
                return false;
            else if (date1.Month > date2.Month)
                return true;
            else if (date1.Month != date2.Month)
                return false;
            else if (date1.Day > date2.Day)
                return true;
            else if (date1.Day != date2.Day)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Compares a DateTime to a Date and see if the DateTime is equal or less than the Date
        /// </summary>
        /// <param name="date1">The date to compare</param>
        /// <param name="date2">The date to compare against</param>
        /// <returns>
        /// True if the DateTime is equal or less than the Date, false otherwise
        /// </returns>
        public static bool operator <=(DateTime date1, Date date2)
        {
            if (date1.Year < date2.Year)
                return true;
            else if (date1.Year != date2.Year)
                return false;
            else if (date1.Month < date2.Month)
                return true;
            else if (date1.Month != date2.Month)
                return false;
            else if (date1.Day < date2.Day)
                return true;
            else if (date1.Day != date2.Day)
                return false;
            else
                return true;
        }
    }
}
